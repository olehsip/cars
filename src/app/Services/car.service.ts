import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class CarService {
  constructor(private http: HttpClient) {
  }

  getCars() {
    return this.http.get('http://localhost:3000/cars');
  }

  addCar(car) {
    const data = {
      name: car.carName,
      color: car.carColor
    };
    return this.http.post('http://localhost:3000/cars', data);
  }

  changeCar(car: any) {

 return this.http.put(`http://localhost:3000/cars/${car.id}`, car);
  }

  deleteCar(car: any) {
    return this.http.delete(`http://localhost:3000/cars/${car.id}`);
  }
}


