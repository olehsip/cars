import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {CarService} from './Services/car.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {BootstrapModalModule} from 'ng2-bootstrap-modal';
import {ConfirmComponent} from './confirm/confirm.component';
import {CarfilterPipe } from './carfilter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    ConfirmComponent,
    CarfilterPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BootstrapModalModule,
    BootstrapModalModule.forRoot({container: document.body})
  ],
  providers: [CarService],
  bootstrap: [AppComponent],
  entryComponents: [
    ConfirmComponent
  ]
})
export class AppModule {
}
