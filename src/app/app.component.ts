import {Component, OnInit, ViewChild} from '@angular/core';
import {CarService} from './Services/car.service';
import {DialogService} from 'ng2-bootstrap-modal';
import {ConfirmComponent} from './confirm/confirm.component';
import {NgForm} from '@angular/forms';

export class Cars {
  name: string;
  color: string;
  id: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('form') form: NgForm;

  constructor(private carService: CarService, private dialogService: DialogService) {
  }

  carname = '';
  cars: Cars [] = [];
  searchCar = '';

  ngOnInit() {
    this.carService.getCars().subscribe((cars: Cars[]) => {
      this.cars = cars;
      console.log(cars);
    });
  }

  showConfirm(car: Cars) {
    let disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: 'Confirm title',
      message: 'Confirm message',
      cars: Object.assign({}, car)
    })
      .subscribe((cars) => {
        if (cars) {
          this.carService.changeCar(cars).subscribe((data: Cars) => {
            this.carService.getCars().subscribe((cars: Cars[]) => {
              this.cars = cars;
              console.log(cars);
            });
          });
        } else {
          alert('You didnt save');
        }

      });
  }

  addCar() {
    {
      this.carService.addCar(this.form.value).subscribe((json: Cars) => {
        this.cars.push(json);
        console.log(json);
      });
      this.carname = '';
    }
  }


  deleteCar(car: Cars) {
    this.carService.deleteCar(car).subscribe(data => {
      this.cars = this.cars.filter(c => c.id !== car.id);
    });
  }
}
