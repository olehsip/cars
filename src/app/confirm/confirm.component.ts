import {Component} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {Cars} from '../app.component';

export interface ConfirmModel {
  title: string;
  message: string;
  cars: Cars;
}

@Component({
  selector: 'confirm',
  templateUrl: './confirm.component.html',
})
export class ConfirmComponent extends DialogComponent<ConfirmModel, Cars> implements ConfirmModel {
  title: string;
  message: string;
  cars: Cars;

  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  confirm() {
    this.result = this.cars;
    this.close();
  }
}
